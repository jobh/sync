#!/bin/bash

# Fixup whitespace changes
for x in $(git status |awk '/modified:/ {print $3}'); do
    git diff -w $x >pat && git checkout HEAD $x && patch -p1 $x <pat
done

# Skip those that differ only in $Date (due to different time zones)
for x in $(git status |awk '/modified:/ {print $3}'); do
    if ! git diff $x|fgrep -v '$Date:' | grep "^[+-][^+-]" >/dev/null; then
	git checkout HEAD $x
    fi
done

