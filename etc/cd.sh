# Improved dir stack (append-only list, really)
# cd -[n]	move (n or 1) down in list
# cd +[n]	move (n or 1) up in list
# cd @[n]       move to entry (n or end) in list
# cd -l|--list	show list
# cd -c|--clear	clear list
# cd [other]	change directory, and add to end of list

unset MY_DIRSTACK MY_I MY_MAXI

declare -a MY_DIRSTACK
declare -i MY_I MY_MAXI MY_OLDI

cd () {
  if [[ $# == 0 ]]; then
    cd ~
    return
  fi
  case "$1" in

    --clear|-c)
	MY_I=0
	MY_MAXI=0
	MY_OLDI=0
	MY_DIRSTACK[0]="$PWD"
	;;

    --list|-l)
        cd -ll | grep -E -C $LINES "(^[*^])|( @ )"
        ;;
    -ll)
	local i=0
        while (( i <= MY_MAXI )); do
	  local rel=" "
	  local abs="$i"
	  if (( i < MY_I && MY_I-i < 10)); then
	    (( MY_I-i > 1 )) && rel="$((MY_I-i))" || rel="-"
	  elif (( i > MY_I && i-MY_I < 10 )); then
	    str="-"
	    (( i-MY_I > 1 )) && rel="$((i-MY_I))" || rel="+"
	  fi
	  if (( i == MY_I )); then
	      rel="*$rel"
	  elif (( i == MY_OLDI )); then
	      rel="^$rel"
	  fi
	  (( i == MY_MAXI )) && abs="@"
	  printf "%2s  %-3s %s\n" "$rel" "$abs" "${MY_DIRSTACK[$i]/#$HOME/~}"
	  ((i++))
        done
	;;

    @|@[0-9]|@[0-9][0-9]) ## Absolute index (as given by --list) (default top)
	local i=${1#@}
	[[ $1 == "@" ]] && i=$MY_MAXI
	if (( i > MY_MAXI )); then
	  echo "beyond end of list" >&2
	  return 1
	fi
	builtin cd "${MY_DIRSTACK[$i]}" || return $?
	MY_OLDI=$MY_I
	MY_I=$i
	;;

    -|-[0-9])	## Move up <n> directories (default 1)
	local i=${1#-}
	[[ $1 == "-" ]] && i=1
	i=$((MY_I-i))
	if (( i < 0 )); then
	  echo "beyond start of list" >&2
	  return 1
	fi
	builtin cd "${MY_DIRSTACK[$i]}" || return $?
	MY_OLDI=$MY_I
	MY_I=$i
	;;

    +|+[0-9])	## Move down <n> directories (default 1)
	local i=${1#-}
	[[ $1 == "+" ]] && i=1
	i=$((MY_I+i))
	if (( i > MY_MAXI )); then
	  echo "beyond end of list" >&2
	  return 1
	fi
	builtin cd "${MY_DIRSTACK[$i]}" || return $?
	MY_OLDI=$MY_I
	MY_I=$i
	;;

    ^)  ## Toggle between two last dirs
	cd @$MY_OLDI
	;;

    -r) ## Replace current
	shift
	builtin cd "$@" || return $?
	[[ ${MY_DIRSTACK[$MY_I]} == $PWD ]] && return 0
	MY_DIRSTACK[$MY_I]="$PWD"
	;;
	
    *)	## A directory name 
	[[ $1 == "--" ]] && shift
	builtin cd "$@" || return $?
	[[ ${MY_DIRSTACK[$MY_I]} == $PWD ]] && return 0
	MY_OLDI=$MY_I
	((MY_I++))	# this would clear list beyond current point
	#MY_I=$((MY_MAXI+1))	# this would append to end of list
	MY_DIRSTACK[$MY_I]="$PWD"
	MY_MAXI=$MY_I
	;;

  esac
}

cd --clear
