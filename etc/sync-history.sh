# Synchronize history between bash sessions
#
# Make history from other terminals available to the current one. However,
# don't mix all histories together - make sure that *all* commands from the
# current session are on top of its history, so that pressing up arrow will
# give you most recent command from this session, not from any session.
#
# Since history is saved on each prompt, this additionally protects it from
# terminal crashes.


export HISTCONTROL=ignoreboth

# truncate main history file
if (( $(stat --printf=%s $HISTFILE) > 1000000 )); then
    tail --lines=10000 $HISTFILE >${HISTFILE}_tmp_$$
    mv ${HISTFILE}_tmp_$$ $HISTFILE
fi

# keep more shell history because it's very useful
export HISTFILESIZE=-1
export HISTSIZE=-1
shopt -s histappend   # don't overwrite history file after each session

# disable automatic writing of HISTFILEX
HISTFILEX=$HISTFILE
unset HISTFILE

# on every prompt, save new history to dedicated file and recreate full history
# by reading all files, always keeping history from current session on top.
function update_history() {
    history -a ${HISTFILEX}.$$
    history -c
    history -r ${HISTFILEX} # load common history file
    # load histories of other sessions
    for f in ${HISTFILEX}.*; do
        [[ $f != ${HISTFILEX}.$$ ]] && history -r $f
    done
    history -r "${HISTFILEX}.$$"  # load current session history
}
if [[ "$PROMPT_COMMAND" != *update_history* ]]; then
    export PROMPT_COMMAND="update_history; $PROMPT_COMMAND"
fi

# merge session history into main history file on bash exit
function merge_session_history() {
    [[ -f ${HISTFILEX}.$1 ]] || return
    cat ${HISTFILEX}.$1 >>$HISTFILEX
    rm -f ${HISTFILEX}.$1
}
trap "merge_session_history $$" EXIT

# detect leftover files from crashed sessions and merge them back
for f in ${HISTFILEX}.*; do
    pid=${f##*.}
    kill -0 "$pid" 2>/dev/null || merge_session_history $pid
done
